import React from "react";

import ExpenseItem from "./ExpenseItem";
import Card from "../UI/Card";
import "./Expenses.css";

const Expenses = (props) => {
  return (
    <Card className="expenses">
      {props.items.map((items) => {
        return (
          <ExpenseItem
            title={items.title}
            amount={items.amount}
            date={items.date}
          />
        );
      })}
    </Card>
  );
};

export default Expenses;
